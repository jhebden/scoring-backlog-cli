# scoring-backlog-cli

This is a really simple CLI for listing out all open issues in a project which are missing either a weight, or a label with a specific prefix (i.e. a sizing label).
It takes in a GitLab project path with `--project`, a label prefix with `--label-prefix`, and a comma-separated list of users who can perform sizing & triage with `--users`.
Each resulting issue which is missing the required label prefix and/or weight has its URL printed, along with the user selected to perform sizing & triage on that issue.

# install

As this is written in Rust, `cargo install` should install everything you need to get running.

# configuration

Ensure `GITLAB_TOKEN` is set in your environment using your secrets management/password management tool of choice.
Only the token, `--project`, `--label-prefix` and `--users` are required for this tool to do something useful.
