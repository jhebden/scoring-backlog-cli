extern crate pretty_env_logger;
#[macro_use]
extern crate log;

use clap::{crate_version, Parser};
use gitlab::{
    api::{issues::IssueState, Query},
    GitlabBuilder,
};
use serde::Deserialize;

/// A super-simple GitLab client to process a backlog of unscored issues
#[derive(Parser, Debug)]
#[command(version = crate_version!(), about = "A supert-simple GitLab issue scoring checker", long_about=None)]
struct Args {
    /// The GitLab project to inspect for open unscored issues
    #[arg(short, long, required = true)]
    project: String,

    /// Comma-separated pool of users to round-robin assign users
    #[arg(short, long, num_args = 1, value_delimiter = ',', required = true)]
    users: Vec<String>,

    /// Check if scoring label is missing using this label prefix
    #[arg(short, long)]
    label_prefix: String,

    /// Also check if weight is missing
    #[arg(short, long, action)]
    check_weight: bool,

    /// GitLab host URI
    #[arg(short, long, default_value = "gitlab.com")]
    gitlab_host: String,

    /// GitLab token
    #[arg(short = 't', long, env = "GITLAB_TOKEN", required = true)]
    gitlab_token: String,
}

/// A GitLab issue with the fields we need
#[derive(Deserialize, Debug, Clone)]
struct GitlabIssue {
    id: i32,
    labels: Vec<String>,
    weight: Option<f32>,
    web_url: String,
}

fn main() {
    // init logger
    pretty_env_logger::init();

    // init clap
    debug!("parsing arguments");
    let args = Args::parse();

    // prepare circular iterator for users
    let mut users = args.users.iter().cycle();

    // get gitlab client
    debug!("creating gitlab client");
    info!("querying GitLab project {} for open issues", args.project);
    let gitlab = match GitlabBuilder::new(args.gitlab_host, args.gitlab_token).build() {
        Ok(gitlab) => gitlab,
        Err(e) => {
            error!("could not build GitLab client: {}", e);
            return;
        }
    };

    // list issues
    debug!("fetching issues");
    let issues_endpoint = match gitlab::api::issues::ProjectIssues::builder()
        .project(args.project.clone())
        .state(IssueState::Opened)
        .build()
    {
        Ok(issues_endpoint) => issues_endpoint,
        Err(e) => {
            error!("could not build project issues query: {}", e);
            return;
        }
    };
    let issues_endpoint_paged = gitlab::api::paged(issues_endpoint, gitlab::api::Pagination::All);

    let project_issues: Vec<GitlabIssue> = match issues_endpoint_paged.query(&gitlab) {
        Ok(project_issues) => project_issues,
        Err(e) => {
            error!("could not query and deserialize project issues: {}", e);
            return;
        }
    };
    debug!("fetched issues: {:?}", project_issues);

    if project_issues.len() > 0 {
        for issue in project_issues.iter() {
            // look for missing weight
            // look for scoring labels
            debug!(
                "checking issue {} for labels with prefix {}",
                issue.id, args.label_prefix
            );
            let mut missing_label = true;
            for label in &issue.labels {
                if label.starts_with(&args.label_prefix) {
                    missing_label = false;
                }
            }

            if issue.weight.is_none() || missing_label {
                // if missing, output issue link and scoring owner, add temp label
                let triage_owner = match users.next() {
                    Some(triage_owner) => triage_owner,
                    None => {
                        error!("ran out of triage owners in circular iterator");
                        return;
                    }
                };
                println!("{}: {}", triage_owner, issue.web_url);
            }
        }
    } else {
        info!("no open issues returned for project {}", args.project);
    }
}
